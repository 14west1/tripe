# tripe

Tripe is a utility that lets you modify a tar file (delete/change files) or just run a script on certain files within the archive in a stream.

**Tripe is Tar-Pipe.** Edit/operate on tar files in a piped stream. Especially useful for large backup files, where you would have to un-tar the contents, do some modifications to file(s) and then re-tar.  For a large backup set, tripe can save a lot of time.

It takes input from stdin, modifies the tar file as it streams by, and writes the modfied tar file to stdout. Examples of usage:

1. Delete huge unwanted files from a backup

`bunzip < backup-big.tbz2 | tripe -d 'big-useless-file' |bzip2 > backup-big-cleaned.tbz2`

2. Scan tar file and create sha256 hash for some files in the archive

`gunzip < archive.tgz | tripe -n -s 'plainfile.txt' 'sha256sum|tr -d "\n";  echo "  $TRIPE_FN"'`

3. Operate on a directory in the tar file, and add a file to the tar based on the directory contents

`gunzip < archive.tgz | tripe -S 'sumDirectory' 'md5sum *.txt > md5sums' | gzip > archive2.tgz`

4. Remove passwords from a backup archive

`gunzip < archive.tgz | tripe -S '/home/bobby/' 'sed -i "s,passw0rd,xxxxxxxx," passwords.c' | gzip > archive2.tgz`

Output of 'tripe -h':

    Tar streaming program.  Takes a tar-format stream on stdin (or a file), and outputs on stdout.
    tripe [options] [tar file]
    If an input tar file name is given, read from that file rather than stdin.
    Options:
      -d regexp  Delete a file/dir where path/filename matches regular expression
      -s regexp  script  Run files matching regexp through script.  Each matching file is piped to the listed script's stdin.
                         Script is passed filename in TRIPE_FN env variable.
      -S regexp  script  Extract dirs matching regexp to temporary area and run script in the directory.
      -n  No content will be sent to stdout.  Only valid with -s or -S
      -v  Report changes made to tar stream.
      -t  tmpdir  Set temp dir for -s and -S options.
      -h  Show this help message.

# Building

One of the following:

    qmake
    make

or

    cmake .
    make
