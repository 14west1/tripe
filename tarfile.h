#ifndef TARFILE_H
#define TARFILE_H
#include <stdio.h>
#include "tar.h"

class TarFile
{
public:
    TarFile(FILE *inputFp);
    TarFile(char *path, char *fileName);
    ~TarFile();
    enum ReadFileErr { rfZeros=-1, rfError=-2 };
    int  readHdr();
    char *fileName()   { return longFileName   ? longFileName   : header.name;}
    char *targetName() { return longTargetName ? longTargetName : header.linkname;}
    bool matches(char *regex);
    char *getDirName();
    bool skipFile(void);                        // Read file contents from input stream -> bitbucket
    bool write(bool hdrOnly);                   // Copy file from input tar stream to output tar stream
    uint64 outputTempFile(char *tempFileName, bool delFile=true);  // Write file header and contents from temp file to output tar stream
    uint64 outputTempFile() { return outputTempFile(xFileName, false); }
    void writeTo(FILE *file, char *filename);   // Write file contents form intput stream into file
    bool writeFile(char *dir);                  // Write file from input stream into temp file, mark with tar file's permissions, etc.
    static void writeTermBlocks();

    bool isDir() { return header.typeflag == DIRTYPE; }

private:
    FILE *input;
    posix_header header;

    // Stuff to handle long filenames (both the file and the link target if symlink)
    bool hasLongName;        // Filename is too long, so it's in block(s) following header
    bool hasLongTarget;  // Filename of link target is too long, so it's in block(s) following header
    char *longFileName;   // If filename is long, this points to long filename
    char *longTargetName; // If link target name is long, this points to long linkname
    posix_header llHdrName;   // Saved header for longLink name
    posix_header llHdrTarget; // Saved header for longTarget name

    // Stuff to handle writing a temp file from disk to tar stream
    char *xFileName;  // File name of real file on disk that will be pushed to tar stream
    void createHeader(char *path, char *filename);

    uint64 fileSize;
    static uint blocksWritten;
    uint64 octalToInt(char *str);
    void intToOctal(uint64 n, char *str, int digits);
    void createCsum(posix_header &phdr);
    void createParentDirs(char *filename);
    void allocStr(char *&str, int len);
};

#endif // TARFILE_H
