#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <unistd.h>
#include <sys/stat.h>
#include <utime.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <sys/sysmacros.h>
#include <ctype.h>
#include "tarfile.h"

extern bool verbose;    // Set for -v
void exitTripe(int signal);
uint TarFile::blocksWritten;

#define DEBUG_TAR_OUTPUT 0
#if DEBUG_TAR_OUTPUT == 1
size_t fwrite_real(const void *ptr, size_t size, size_t nmemb,
                     FILE *stream);
#define fwrite(a, b, c, d) dbgfwrite(a, b, c, d)
uint dumpAddr = 0;
size_t dbgfwrite(const void *ptr, size_t size, size_t nmemb,
                     FILE *stream)
{
    if (stream == stdout)
    {
        // Send hex dump to stderr
        uchar *p = (uchar*)ptr;
        int len = size * nmemb;
        fprintf(stderr, "\n");
        while (len)
        {
            int blen = len >= 16 ? 16 : len;
            char dumpStr[17] = {0};
            fprintf(stderr, "%08X  ", dumpAddr);
            dumpAddr += 16;
            for (int i=0;i<blen;i++)
            {
                char c = '.';
                if (isprint(*p))
                    c = *p;
                dumpStr[i] = c;
                fprintf(stderr, "%02X ", *p++);
            }
            fprintf(stderr, " %s\n", dumpStr);
            len -= blen;
        }
    }
    return fwrite_real(ptr, size, nmemb, stream);
}
#endif

// Create tar file object which will be used to read a tar file from input stream
TarFile::TarFile(FILE *inputFp) : input(inputFp), fileSize(0)
{
    hasLongName = 0;
    longFileName = 0;
    hasLongTarget = 0;
    longTargetName = 0;
    xFileName = 0;
    memset(&header, 0, sizeof(header));
    memset(&llHdrName, 0, sizeof(llHdrName));
    memset(&llHdrTarget, 0, sizeof(llHdrTarget));
}

/* Create a tar file object which will be used to write a physical file from
   temporary area (using -S option)to tar stream on stdout */
TarFile::TarFile(char *path, char *fileName) : fileSize(0)
{
    input = 0;
    hasLongName = 0;
    longFileName = 0;
    hasLongTarget = 0;    longTargetName = 0;
    memset(&header, 0, sizeof(header));
    memset(&llHdrName, 0, sizeof(llHdrName));
    memset(&llHdrTarget, 0, sizeof(llHdrTarget));
    xFileName = fileName;
    createHeader(path, fileName); //this should be rel path/name for file in archive
}

TarFile::~TarFile()
{
    free(longFileName);
    free(longTargetName);
}

/* Read input file from stdin, it's not being written to output, but we need to
   get to the next file */
bool TarFile::skipFile()
{
    // Skip past the file blocks (we have already read the header)
    uint64 bytesLeft = fileSize;
    // Pad out block
    int partial = fileSize % BLOCK_SIZE;
    if (partial)
        bytesLeft += BLOCK_SIZE - partial;
    char buf[BLOCK_SIZE];
    while (bytesLeft > 0)
    {
        int toread = BLOCK_SIZE;
        if (bytesLeft < BLOCK_SIZE)
            toread = bytesLeft;
        int n = fread(buf, 1, toread, input);
        if (n <= 0)
        {
            // Failed to read input
            fprintf(stderr, "ERROR: Failed to read input\n");
            exitTripe(0);
        }
        bytesLeft -= n;
    }
    //fprintf(stderr, "Skipping file %s, %d bytes\n", fileName(), (int)fileSize);
    return true;
}

/** Read the file header from stdin, store it in header class var
    @return Size of file (which can be zero), or ReadFileErr
*/
int TarFile::readHdr()
{
    // Read header
    if (fread(&header, BLOCK_SIZE, 1, input) != 1)
            return rfError;

    // Check for zero header
    char *p = (char*)&header;
    int i;
    for (i=0;i<BLOCK_SIZE;i++)
        if (*p++)
            break;
    if (i == BLOCK_SIZE)
        return rfZeros;

    // Will always need file size
    fileSize = octalToInt(header.size);

    // Check for LongLink (long filename).
    if (!strcmp(fileName(), LONGLINK_NAME))
    {
        // Long filename, or link target is long filename
        uint blocks = (fileSize + BLOCK_SIZE -1) / BLOCK_SIZE;
        if (header.typeflag == GNUTYPE_LONGLINK)
        {
            hasLongTarget = true;
            // Allocate memory
            allocStr(longTargetName, blocks * BLOCK_SIZE);
            // Save input header
            llHdrTarget = header;
            // Read in filename
            if (fread(longTargetName, BLOCK_SIZE, blocks, input) != blocks)
                return rfError;
            // read in Next header
        }
        if (header.typeflag == GNUTYPE_LONGNAME)
        {
            hasLongName = true;
            // Allocate memory
            allocStr(longFileName, blocks * BLOCK_SIZE);
            // Save input header
            llHdrName = header;
            // Read in filename
            if (fread(longFileName, BLOCK_SIZE, blocks, input) != blocks)
                return rfError;
            // read in Next header
        }
        // Done with pre-header with long name, no read next header
        return readHdr();
    }

    // Check checksum
    posix_header hdr = header;
    memset(hdr.chksum, ' ', sizeof(hdr.chksum));
    int csum=0;
    p = (char*)&hdr;
    for (uint i=0;i<sizeof(posix_header);i++)
        csum += *p++;
    int csHdr = octalToInt(header.chksum);
    if (csum != csHdr)
    {
        fprintf(stderr, "Header checksum bad, file = %s\n",
                fileName());
    }
    return 0;
}

// Conversion function needed because tar header uses octal strings for numbers
uint64 TarFile::octalToInt(char *str)
{
    uint64 n = strtoull(str, 0, 8);
    return n;
}

// Conversion function needed because tar header uses octal strings for numbers
void TarFile::intToOctal(uint64 n, char *str, int digits)
{
    // Awkward baloney to suppress compiler warnings
    char fs1[] = "%0*lo";
    char fs2[] = "%0*llo";

    if (sizeof(long) == 8)
        sprintf(str, fs1, digits-1, n);
    else
        sprintf(str, fs2, digits-1, n);
}

/* Create valid checksum for the posix tar header. This is done by adding all
   the chars together with the chksum field set to all spaces */
void TarFile::createCsum(posix_header &phdr)
{
    // Write new csum
    memset(phdr.chksum, ' ', sizeof(phdr.chksum));
    int csum = 0;
    char *p = (char*)&phdr;
    for (uint i=0;i<sizeof(posix_header);i++)
        csum += *p++;
    char csStr[25];
    intToOctal(csum, csStr, sizeof(phdr.chksum));
    strncpy(phdr.chksum, csStr, sizeof(phdr.chksum));
}

// Create dir path needed for a file to be written to the temp area (-S option)
void TarFile::createParentDirs(char *filename)
{
    // Create parent dirs
    int len = strlen(filename);
    char *path = (char*)alloca(len + 2);
    strncpy(path, filename, len+1);
    // Make sure it's not just a filename with no path
    if (!strchr(filename, '/'))
        // No dirs to make
        return;
    // back up over file
    char *p = path + len - 1;
    while(p > filename)
    {
        if (*p == '/')
        {
            *p = 0;
            break;
        }
        p--;
    }
    len = strlen(path);
    int x = 0;
    while (x < len+1)
    {
        if (path[x] == '/' || path[x] == 0)
        {
            // Check that dir exists
            path[x] = 0;
            DIR *d = opendir(path);
            if (d)
                closedir(d);
            else
            {
                // Create dir if possible
                if (mkdir(path, 0755))
                {
                    fprintf(stderr, "Cannot create temp dir %s\n", path);
                    exitTripe(0);
                }
            }
            // Keep going.
            path[x] = '/';
        }
        x++;
    }
}

// Allocate/free a memory buffer for a string
void TarFile::allocStr(char *&str, int len)
{
    if (str)
        free(str);
    str = (char*)malloc(len);
    memset(str, 0, len);
    if (!str)
    {
        fprintf(stderr, "ERROR: allocating %d bytes for string.\n", len);
        exit(1);
    }
}

/* Create Tar header for file, saved in this class's header element. This is
   called when we are writing a temp file (-S or -s option) to tar stream on stdout */
void TarFile::createHeader(char *path, char *filename)
{
    struct stat statBuf;
    if (lstat(filename, &statBuf))
    {
        fprintf(stderr, "Error: Cannot stat file %s\n", filename);
        exitTripe(0);
    }

    // Create full path/filename
    const int pathLen = strlen(path);
    const int fnLen   = strlen(filename);
    const int fullSize = pathLen + fnLen + 2;
    char *fullName = (char*)malloc(fullSize);
    strncpy(fullName, path, pathLen + 1);
    strncat(fullName, filename, fnLen + 1);
    memset(&header, 0, sizeof(header));

    intToOctal(statBuf.st_mode & 07777, header.mode, 8);
    intToOctal(statBuf.st_uid,  header.uid,  8);
    intToOctal(statBuf.st_gid,  header.gid,  8);
    intToOctal(0, header.size, 12);
    intToOctal(statBuf.st_mtim.tv_sec, header.mtime, 12);
    memcpy(header.magic, TMAGIC, TMAGLEN);
    memcpy(header.version, TVERSION, TVERSLEN);

    switch (statBuf.st_mode & S_IFMT)
    {
    case S_IFREG: // Regular file (or hard link)
        header.typeflag = REGTYPE;
        intToOctal(statBuf.st_size, header.size, 12);
        break;
    case S_IFDIR: // Directory
        header.typeflag = DIRTYPE;
        strncat(fullName, "/", 2);
        break;
    case S_IFIFO: // FIFO file
        header.typeflag = FIFOTYPE;
        break;
    case S_IFLNK: // Symlink
    {
        // Now we have to get the link target's name
        char name[PATH_MAX] = {0};
        int len;
        if ((len = readlink(filename, name, PATH_MAX)) <= 0)
        {
            fprintf(stderr, "Cannot read link %s\n", filename);
            exitTripe(0);
        }
        // Check for long target name
        if (len > NAME_LEN)
        {
            // Long target name
            hasLongTarget = true;
            uint blocks = (len + 1 + BLOCK_SIZE - 1) / BLOCK_SIZE;
            allocStr(longTargetName, blocks * BLOCK_SIZE);
            strncpy(longTargetName, name, len+1);
        }
        // Save name in linkname in header (or at least first 100 bytes)
        strncpy(header.linkname, name, NAME_LEN);
        header.typeflag = SYMTYPE;
    }
        break;
    case S_IFCHR:
        header.typeflag = CHRTYPE;
        break;
    case S_IFBLK:
        header.typeflag = BLKTYPE;
        break;
    default:
        fprintf(stderr, "ERROR: File \"%s\" has invalid file type %d.\n",
                filename, statBuf.st_mode & S_IFMT);
        exit(1);
        break;
    }

    // Handle filename, includeing long file name
    strncpy(header.name, fullName, NAME_LEN);
    // Handle long filename
    if (fullSize > NAME_LEN)
    {
        // Need extended long name header
        hasLongName = true;
        uint blocks = (fullSize + BLOCK_SIZE - 1) / BLOCK_SIZE;
        allocStr(longFileName, blocks * BLOCK_SIZE);
        strncpy(longFileName, fullName, fullSize);
    }

    free(fullName);
    // Magic value and version
    strncpy(header.magic, TMAGIC, TMAGLEN);
    memcpy(header.version, TVERSION, TVERSLEN);
    // User name and group name
    struct passwd *pwline =  getpwuid(statBuf.st_uid);
    strncpy(header.uname, pwline->pw_name, 32);
    struct group *grpline = getgrgid(statBuf.st_gid);
    strncpy(header.gname, grpline->gr_name, 32);

    // Create header checksum
    createCsum(header);
}

/* Write extra blocks filled with zeros to end of tar file. There must be at
   least two blocks, and then enough to make whole file size be a multiple of the
   record size (lookup BLOCKING_FACTOR). Linux defaults to a record size of 20
   512-byte blocks. */
void TarFile::writeTermBlocks()
{
    // Write at least two block of zeros, plus padding to make total blocks a
    // multiple of twenty, as is custom
    char zeros[BLOCK_SIZE];
    memset(zeros, 0, BLOCK_SIZE);
    for (int i=0;i<BLOCKING_FACTOR + 2;i++)
    {
        if (fwrite(zeros, BLOCK_SIZE, 1, stdout) != 1)
            fprintf(stderr, "ERROR: failed to write 512 zero bytes to stdout\n");
        blocksWritten++;
        if (i > 0 && (blocksWritten % BLOCKING_FACTOR) == 0)
            // Wrote at least two blocks, plus we are on 10K boundary
            break;
    }        
}

/* Write given file to tar stream, including header. This happens after script
   has run, and resulting files are read from temp storage and written to stdout
   with tar header */
uint64 TarFile::outputTempFile(char *tempFileName, bool delFile)
{
    // Get file size of new file
    struct stat statBuf;
    if (lstat(tempFileName, &statBuf))
    {
        fprintf(stderr, "Error: Cannot stat file %s\n", tempFileName);
        exitTripe(0);
    }

    // Handle long-name header(s)
    posix_header *longHdr;
    if (hasLongName || hasLongTarget)
    {
        // Create header for long block
        longHdr = (posix_header*)alloca(sizeof(posix_header));
        memset(longHdr, 0, sizeof(posix_header));
        strncpy(longHdr->name, LONGLINK_NAME, NAME_LEN);
        intToOctal(0644, longHdr->mode, 8);
        intToOctal(0,    longHdr->uid, 8);
        intToOctal(0,    longHdr->gid, 8);
        intToOctal(0,    longHdr->mtime, 12);
        strncpy(longHdr->magic, header.magic, TMAGLEN);
        strncpy(longHdr->version, header.version, TVERSLEN);
        strncpy(longHdr->uname, header.uname, 32);
        strncpy(longHdr->gname, header.gname, 32);
        // long-name blocks are sent first
        if (hasLongTarget)
        {
            // Add in size, create checksum, send hdr, send name
            intToOctal(strlen(longTargetName)+1, longHdr->size, 12);
            longHdr->typeflag = GNUTYPE_LONGLINK;
            createCsum(*longHdr);
            // Save long target header, write() will output header/long name
            memcpy(&llHdrTarget, longHdr, sizeof(posix_header));
        }
        if (hasLongName)
        {
            // Add in size, create checksum, send hdr, send name
            intToOctal(strlen(longFileName)+1, longHdr->size, 12);
            longHdr->typeflag = GNUTYPE_LONGNAME;
            createCsum(*longHdr);
            // Save long target header, write() will output header/long name
            memcpy(&llHdrName, longHdr, sizeof(posix_header));\
        }
    }

    // Set size in header
    uint hdrSize = 0;
    if (header.typeflag == REGTYPE || header.typeflag == AREGTYPE)
        hdrSize = statBuf.st_size;
    intToOctal(hdrSize, header.size, sizeof(header.size));

    // Compute new header csum
    createCsum(header);

    // Write header(s) out
    write(true);

    // Not all files are plain files
    if ((statBuf.st_mode & S_IFMT) != S_IFREG)
        return 0;

    // Regular file, Read chunks, write them out
    uint64 bytes=0;
    FILE *tempFile = 0;
    tempFile = fopen(tempFileName, "r");
    if (!tempFile)
    {
        fprintf(stderr, "Temp file does not exist.\n");
        exitTripe(0);
    }

    for(;;)
    {
        char buf[BLOCK_SIZE];
        memset(buf, 0, BLOCK_SIZE);
        int readSize = fread(buf, 1, BLOCK_SIZE, tempFile);
        if (readSize == 0)
            // No more to read
            break;
        // write entire block (with padding zeros) to output (tar file)
        int writeSize = fwrite(buf, BLOCK_SIZE, 1, stdout);
        if (writeSize != 1)
        {
            fprintf(stderr, "Write error, block not written.\n");
            exitTripe(0);
        }
        blocksWritten++;
        bytes += readSize;
    }
    if (tempFile)
        fclose(tempFile);
    // All done
    if (delFile)
        unlink(tempFileName);
    return bytes;
}

// Write the file contents for this file from the tar stream into a file
void TarFile::writeTo(FILE *file, char *filename)
{
    // Write the file contents to file
    uint64 bytesLeft = fileSize;
    char buf[BLOCK_SIZE];
    while(bytesLeft)
    {
        // Read next block in
        if (fread(buf, 1, BLOCK_SIZE, input) == 0)
        {
            // Failed to read input
            fprintf(stderr, "ERROR (writeTo): Failed to read input\n");
            exitTripe(0);
        }
        uint writeSize = BLOCK_SIZE;
        if (bytesLeft < BLOCK_SIZE)
            writeSize = bytesLeft;
        // Write to output
        if (fwrite(buf, writeSize, 1, file) != 1)
            fprintf(stderr, "ERROR: Failed to write to file \"%s\"\n",
                    filename);
        // Don't increment blocksWritten here, this isn't writing to tar stream
        bytesLeft -= writeSize;
    }
}

// Write file to temp dir, using tarfile's permissions
// Return true if file written was a dir.
bool TarFile::writeFile(char *dir)
{
    bool retval=false;
    if (chdir(dir))
    {
        fprintf(stderr, "Cannot change to dir %s\n", dir);
        exitTripe(0);
    }

    // Make all the dirs in path up to file
    createParentDirs(fileName());

    // Write file
    switch (header.typeflag)
    {
    case REGTYPE:           /* regular file */
    case AREGTYPE:          /* regular file */
    {
        char *filename = fileName();

        FILE *file = fopen(filename, "w");
        if (!file)
        {
            fprintf(stdout, "Cannot open file %s for writing.\n", filename);
            exitTripe(0);
        }
        // Write the file
        writeTo(file, filename);
        fclose(file);
        // Chmod the file
        mode_t mode = octalToInt(header.mode);
        chmod(filename, mode);
        // Set user and group
        if (chown(filename, octalToInt(header.uid), octalToInt(header.gid)) == -1)
            fprintf(stderr, "ERROR: Failed to change owner for file \"%s\"\n",
                    filename);
        // Set time
        uint time = octalToInt(header.mtime);
        struct utimbuf utb = { __time_t(time), __time_t(time) };
        utime(filename, &utb);
    }
        break;
    case LNKTYPE:           /* link */
        // Create link
        if (link(targetName(), fileName()))
        {
            fprintf(stderr, "Cannot create hard link to %s named %s\n",
                    targetName(), fileName());
            exitTripe(0);
        }
        break;
    case SYMTYPE:           /* symlink */
        if (symlink(targetName(), fileName()))
        {
            fprintf(stderr, "Cannot create symlink to %s named %s\n",
                    targetName(), fileName());
            exitTripe(0);
        }
        break;
    case CHRTYPE:           /* character special */
    case BLKTYPE:           /* block special */
    {
        // Use mknod() to create a character special device node
        mode_t mode = octalToInt(header.mode);
        // Permissions are OR'd with mode argument
        mode |= header.typeflag == CHRTYPE ? S_IFCHR : S_IFBLK;
        dev_t dev = makedev(octalToInt(header.devmajor), octalToInt(header.devminor));
        if (mknod(fileName(), mode, dev) != 0)
        {
            fprintf(stderr, "Failed to write Char Special \"%s\" to temp dir\n",
                    fileName());
            fprintf(stderr, "You must run as root to write this file.\n");
            exit(1);
        }
    }
        break;
    case DIRTYPE:           /* directory */
    {
        char *filename = fileName();
        // Set file attributes based on TAR header
        // Chmod the file
        mode_t mode = octalToInt(header.mode);
        chmod(filename, mode);
        // Set user and group
        if (chown(filename, octalToInt(header.uid), octalToInt(header.gid)) == -1)
            fprintf(stderr, "ERROR: Failed to change owner for file \"%s\"\n",
                    filename);
        // Set time
        uint time = octalToInt(header.mtime);
        struct utimbuf utb = { __time_t(time), __time_t(time) };
        utime(filename, &utb);
        retval = true;
    }
        break;
    case FIFOTYPE:          /* FIFO special */
        if (mkfifo(fileName(), octalToInt(header.mode)))
        {
            fprintf(stderr, "Cannot create FIFO %s\n", fileName());
            exitTripe(0);
        }
        break;
    default:
        fprintf(stderr, "ERROR: File \"%s\" has invalid typeflag = %d\n",
                fileName(), header.typeflag);
        exit(1);
        break;
    }
    return retval;
}

// See if filename matches regex
bool TarFile::matches(char *regex)
{
    regex_t re;
    char *filename = fileName();
    if (!regex)
        return false;
    if (regcomp(&re, regex, REG_EXTENDED))
    {
        // can't understand regex
        fprintf(stderr, "Can't parse regex '%s'\n", regex);
        return false;
    }

    bool retval = true;
    if (regexec(&re, filename, 0, 0, 0))
        retval = false;
    regfree(&re);
    return retval;
}

/* Return allocated string containing the directory name of file
 * CALLER MUST FREE THE RETURNED POINTER
 */
char *TarFile::getDirName()
{
    char *filename = fileName();
    char *lastSlash = rindex(filename, '/');
    int len;
    if (lastSlash)
        len = lastSlash - filename;
    else
        len = strlen(filename);
    char *s = (char*)malloc(len+1);
    strncpy(s, filename, len);
    s[len] = 0;
    return s;
}

/* Write tar header and, optionall, file contents from input stdin stream to
   output tar stream on stdout */
bool TarFile::write(bool hdrOnly)
{
    // Write out a long link target first
    if (hasLongTarget)
    {
        // Write saved header, and then write long name block
        int nameLen = octalToInt(llHdrTarget.size);
        uint blocks = (nameLen + BLOCK_SIZE -1) / BLOCK_SIZE;
        // Write Long Link header
        if (fwrite(&llHdrTarget, BLOCK_SIZE, 1, stdout) != 1)
            fprintf(stderr, "ERROR: Failed to write to stdout\n");
        blocksWritten++;
        // Write block(s) with long name
        if (fwrite(longTargetName, BLOCK_SIZE, blocks, stdout) != blocks)
            fprintf(stderr, "ERROR: Failed to write to stdout\n");
        blocksWritten += blocks;
    }
    // Write out a long link filename
    if (hasLongName)
    {
        // Write saved header, and then write long name block
        int nameLen = octalToInt(llHdrName.size);
        uint blocks = (nameLen + BLOCK_SIZE -1) / BLOCK_SIZE;
        // Write Long Link header
        if (fwrite(&llHdrName, BLOCK_SIZE, 1, stdout) != 1)
            fprintf(stderr, "ERROR: Failed to write to stdout\n");
        blocksWritten++;
        // Write block(s) with long name
        if (fwrite(longFileName, BLOCK_SIZE, blocks, stdout) != blocks)
            fprintf(stderr, "ERROR: Failed to write to stdout\n");
        blocksWritten += blocks;
    }

    // Write header
    if (fwrite(&header, sizeof(header), 1, stdout) != 1)
        fprintf(stderr, "ERROR: Failed to write to stdout\n");
    blocksWritten++;

    if (hdrOnly)
        return true;

    if (!fileSize) return true;

    // Just read/write from in to out
    uint blocks = (fileSize + BLOCK_SIZE - 1)/ (uint64)BLOCK_SIZE;

    char buf[BLOCK_SIZE];
    while (blocks)
    {
        int len = fread(buf, BLOCK_SIZE, 1, input);
        if (len != 1)
            return false;
        if (fwrite(buf, BLOCK_SIZE, 1, stdout) != 1)
            fprintf(stderr, "ERROR: Failed to write to stdout\n");
        blocksWritten++;
        blocks--;
    }
    return true;
}
