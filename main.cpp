#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <signal.h>
#include "tarfile.h"

/*
  Todo:
  Save other file types from tar file
  DONE Handle hard links and other special types in outputTempFile
  Maybe: If a file has > 1 links, then other links can be hard links.  Order doesn't matter.
  createParentDirs should chmod/chown/touch the new directory (the last one after the final /)
 */
/**
  Tar streaming program.  Takes a tar-format stream on stdin, and outputs on stdout.

  Can modify a tar file in a stream.

  Options:
    -d regexp  Delete a file/dir where path/filename matches regular expression
    -s regexp  script  Run files matching regexp through script.  Each matching file is piped to the listed script's stdin.
    -S regexp  script  Extract dirs matching regexp to temporary area and run script in the directory.
    -n If used, no output is emitted to stdout
    -v Report changes to tar.
    -p Preserve permissions, ownership, etc for files when using -S option.
    -i Ignore all files except every nth file
*/

/*
  Usage of temporary area, which is used for -s and -S options.
  The temporary area is in /dev/shm, which is a filesystem stored in RAM.
  There will be an option to specify the temp area is /dev/shm won't work.

  When using the -s option, a single file is piped into a process running the
  script that the user specifies. If the -n option is not invoked, the script's
  output will be piped to the tar file output on stdout. The file name will
  look like this:
  /dev/shm/tripe-98273987

  When using the -S option, the matched directory is stored in temp dir like
  /dev/shm/tripe-98273833/sqldata

  When the -S dir is "/" then the root dir of the tar file is saved to the temp
  dir, and then the script is operated on it.
*/

bool noOutput;       // Set for -n
bool verbose;        // Set for -v
bool preserve;       // Set for -p
char tmpDirRoot[256] = "/dev/shm/";  // Temporary directory
char *tmpDirName;    // Temporary dir for tarfile, "/dev/shm/tripe-234233/"
char *tmpFileName;   // Temporary file name for single file (-s option) "/dev/shm/tripe-847873"
int  lastDirLen;    // Save length of last dir written, used to prevent writing it again
int  tmpDirLength;
static int  sigCaught;

void makeTempFilename();
void makeTempDirname();
void setupSignalHandler();
void signalHandlerFunc(int signal);
void exitTripe(int signal);

void usage(char *msg = 0)
{
    fprintf(stderr, "Tar streaming program.  Takes a tar-format stream on stdin (or a file), and outputs on stdout.\n");
    fprintf(stderr, "tripe [options] [tar file]\n");
    fprintf(stderr, "If an input tar file name is given, read from that file rather than stdin.\n");
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "  -d regexp  Delete a file/dir where path/filename matches regular expression\n");
    fprintf(stderr, "  -s regexp  script  Run files matching regexp through script.  Each matching file is piped to the listed script's stdin.\n");
    fprintf(stderr, "                     Script is passed filename in TRIPE_FN env variable.\n");
    fprintf(stderr, "  -S regexp  script  Extract dirs matching regexp to temporary area and run script in the directory.\n");
    fprintf(stderr, "  -i N               Ignore files (skip) except for every Nth file.\n");
    fprintf(stderr, "  -n  No content will be sent to stdout.  Only valid with -s or -S\n");
    fprintf(stderr, "  -v  Report changes made to tar stream.\n");
    //fprintf(stderr, "  -p  Preserve permissions, ownership, etc for files when using -S option.\n");
    fprintf(stderr, "  -t  tmpdir  Set temp dir for -s and -S options.\n");
    fprintf(stderr, "  -h  Show this help message.\n");
    if (msg)
        fprintf(stderr, "\nERROR: %s\n", msg);
    exit(msg != 0);
}

/* Write contents of the temporary directory to the tar stream. This function
   is recursive, as this function only writes the dir and all files in it.
   @param dirName The full path to the directory to write
   @param inputPath The path beyond dirName, the path that will show up in tar output
 */
static void writeTempDirToTarStream()
{
    // find all files in tmpdir, and save to tar stream
    char *currentPath = (char*)malloc(PATH_MAX);
    if (!getcwd(currentPath, PATH_MAX))
    {
        fprintf(stderr, "Failed to get current directory.\n");
        exit(1);
    }
    strncat(currentPath, "/", 2);
    char *tarPath = currentPath + tmpDirLength;
    DIR *dir = opendir(currentPath);
    if (!dir)
    {
        fprintf(stderr, "Error opening dir %s\n", currentPath);
        exit(1);
    }

    struct dirent *dirEntry;
    while ((dirEntry = readdir(dir)) != 0)
    {
        if (sigCaught)
            exitTripe(sigCaught);
        // Get next entry
        char *filename = dirEntry->d_name;
        const int entryNameLen = strlen(filename);
        if (entryNameLen > 255)
        {
            fprintf(stderr, "File name too long: %s/%s\n",  tarPath, filename);
            exitTripe(0);
        }
        // Handle subdirs
        if (dirEntry->d_type == DT_DIR)
        {
            // Skip . and ..
            if ((!strcmp(filename, "." ) ||
                 !strcmp(filename, "..")))
                continue;
            // Must write directory entry
            if (strlen(tarPath) >= (uint)lastDirLen)
            {
                TarFile nextDir(tarPath, filename);
                nextDir.outputTempFile();
            }
            // Recursively handle subdirectory
            chdir(filename);
            writeTempDirToTarStream();
            // Return to original directory
            if (chdir(currentPath) != 0)
            {
                fprintf(stderr, "ERROR: Cannot change to dir \"%s\"\n",
                        currentPath);
                exit(1);
            }
            continue;
        }
        // Handle this file
        TarFile tf(tarPath, filename);
        tf.outputTempFile();
    }
    closedir(dir);
    free(currentPath);
}

void processScriptOnMatchedDir(char *script, char *tmpDirName, char *mDir)
{
    // We are done saving a matched dir to temp file, now process
    // the temp files through script, and save the dir to stream

    // This function is called once for the temp dir in tmpDirName. All
    // files/paths in tmp dir have the same path as they have in the tar file,
    // so no name remembering or manipulation is required.

    // The mDir var is the name of the directory that was matched, and the
    // script must be run in that dir.

    // Run Script on saved files
    int cmdLen = strlen(script);
    char *cmdStr = (char*)alloca(cmdLen + 20);
    strncpy(cmdStr, script, cmdLen + 1);
    // Change to tempdir
    if (chdir(tmpDirName) || chdir(mDir))
    {
        fprintf(stderr, "Cannot change to temp dir %s\n", tmpDirName);
        exitTripe(0);
    }
    FILE *pipe = popen(cmdStr, "r");
    char buf[BLOCK_SIZE];
    int len;
    while ((len = fread(buf, 1, BLOCK_SIZE-1, pipe)) != 0)
    {
        // Echo script output to stderr
        buf[len] = 0; // asciiz terminate string
        fprintf(stderr, "%s", buf);
    }
    // Wait for script to finish
    pclose(pipe);

    // Save files to output stream (unless nochange is set)
    if (!noOutput)
    {
        chdir(tmpDirName);
        writeTempDirToTarStream();
    }
    // Delete tmp dir
    sprintf(cmdStr, "rm -rf %s", tmpDirName);
    len = system(cmdStr);
}

int main(int argc, char *argv[])
{
    FILE *in = stdin;
    char *delRegex = 0;
    char *scriptRegex = 0;
    char *script;
    char *matchedDir = 0;
    bool entireDir = false;    // True for -S option (not -s)
    bool inMatchedDir = false;
    int  skip = 0;

    // Setup signal handler
    setupSignalHandler();

    // Handle args
    argv++;  // Skip argv[0]
    argc--;
    while (argc > 0)
    {
        if (*argv[0] == '-')
        {
            if (!strncmp(*argv, "-d", 2))
            {
                if (argc < 2) usage();
                if (delRegex)
                    usage((char*)"Can only have one -d option");
                delRegex = *++argv;
                argc--;
            }
            if (!strncmp(*argv, "-s", 2))
            {
                if (argc < 3) usage();
                if (scriptRegex)
                    usage((char*)"Can only have one of -s or -S options");
                scriptRegex = *++argv;
                script = *++argv;
                argc -= 2;
            }
            if (!strncmp(*argv, "-S", 2))
            {
                entireDir = true;
                if (argc < 3) usage();
                if (scriptRegex)
                    usage((char*)"Can only have one of -s or -S options");
                scriptRegex = *++argv;
                script = *++argv;
                argc -= 2;
            }
            if (!strncmp(*argv, "-i", 2))
            {
                if (argc < 2) usage();
                skip = atol(*++argv);
                if (!skip) usage((char*)"Skip (-i) needs a non-zero argumentf");
                argc--;
            }
            if (!strncmp(*argv, "-n", 2))
                noOutput = true;
            if (!strncmp(*argv, "-v", 2))
                verbose = true;
            if (!strncmp(*argv, "-p", 2))
                preserve = true;
            if (!strncmp(*argv, "-t", 2))
            {
                // Set tmp dir
                char *tmpDir = *++argv;
                argc -= 2;
                uint len = strlen(tmpDir);
                if (len > sizeof(tmpDirRoot)-2 || len == 0)
                {
                    char msg[100];
                    sprintf(msg, "Temp path length must be non-zero and < %d bytes.", (int)sizeof(tmpDirRoot));
                    usage(msg);
                }
                strncpy(tmpDirRoot, tmpDir, sizeof(tmpDirRoot));
                // Add / to end of path
                if (tmpDirRoot[strlen(tmpDirRoot) - 1] != '/')
                    strcat(tmpDirRoot, "/");
            }
            if (!strncmp(*argv, "-h", 2))
                usage();
        }
        else
        {
            in = fopen(argv[0], "r");
            if (!in)
            {
                fprintf(stderr, "ERROR: Cannot open file \"%s\"\n", argv[0]);
                usage();
            }
        }
        argv++;
        argc--;
    }

    // Read files from archive
    int fileNdx = 0;
    for(;;)
    {
        if (sigCaught)
            exitTripe(sigCaught);
        TarFile *tarFile = new TarFile(in);
        int fileLen = tarFile->readHdr();
        if (fileLen == TarFile::rfError)
        {
            fprintf(stderr, "Read error\n");
            exitTripe(0);
        }
        if (fileLen == TarFile::rfZeros)
            // All done
            break;

        // Check for ignore flag
        if (skip && !entireDir)
        {
            if (fileNdx++ % skip)
            {
                // Skip over file blocks in source file
                tarFile->skipFile();
                if (verbose)
                    fprintf(stderr, "Skipping file '%s'\n", tarFile->fileName());
                delete tarFile;
                tarFile = 0;
                continue;
            }
        }

        // Check for file delete
        if (tarFile->matches(delRegex))
        {
            // Skip over file blocks in source file
            tarFile->skipFile();
            if (verbose)
                fprintf(stderr, "Deleting file '%s' from archive\n", tarFile->fileName());
            delete tarFile;
            tarFile = 0;
            continue;
        }

        // Check for path/filename matching regex for script
        if (inMatchedDir)
        {
            // See if we're out of the matched dir yet
            if (!tarFile->matches(scriptRegex))
            {
                processScriptOnMatchedDir(script, tmpDirName, matchedDir);
                inMatchedDir = false;
                if (matchedDir)
                {
                    free(matchedDir);
                    matchedDir = 0;
                }
            }
        }

        if (tarFile->matches(scriptRegex))
        {
            if (entireDir)
            {
                matchedDir = tarFile->getDirName();
                // Setup temp dir
                if (!inMatchedDir)
                {
                    inMatchedDir = true;
                    // Create temporary dir name -> tmpDirName
                    makeTempDirname();
                    if (mkdir(tmpDirName, 0755))
                    {
                        fprintf(stderr, "Error creating temp dir %s\n", tmpDirName);
                        exitTripe(0);
                    }
                    if (verbose)
                        fprintf(stderr, "Creating temp dir %s\n", tmpDirName);
                    if (chdir(tmpDirName))
                    {
                        fprintf(stderr, "Error changing to directory %s\n", tmpDirName);
                        exitTripe(0);
                    }
                }
                if (verbose)
                    fprintf(stderr, "Saving file %s to temp dir\n", tarFile->fileName());
                // Save to temp file
                tarFile->writeFile(tmpDirName);
                // File has been written, and skipped
            }
            else if (noOutput) // Just one file, pipe out only to script
            {
                // Just run pipe script on file output
                if (verbose)
                    fprintf(stderr, "Piping file %s to script.\n", tarFile->fileName());

                // Set TRIPE_FN env var so script knows filename
                setenv("TRIPE_FN", tarFile->fileName(), 1);

                // Run script on file
                char *cmdStr = (char*)alloca(10 + strlen(scriptRegex));
                sprintf(cmdStr, "%s", script);

                // Run the script
                FILE *pipe = popen(cmdStr, "w");

                // Write file to pipe
                tarFile->writeTo(pipe, (char*)"pipe");

                // Wait for script to finish running
                pclose(pipe);
            }
            else // Just one file, pipe runs on file, saved to temp file, then output to tar stream
            {
                // Pipe input stream into script, which pipes into temp file using popen().
                // Then save temp file into output stream
                if (verbose)
                    fprintf(stderr, "Piping file %s to script.\n", tarFile->fileName());

                // Run script on file, and pipe output to temp file
                makeTempFilename();
                char *cmdStr = (char*)alloca(strlen(tmpFileName) + 10 + strlen(scriptRegex));
                sprintf(cmdStr, "%s > %s", script, tmpFileName);

                // Run the script
                FILE *pipe = popen(cmdStr, "w");
                if (pipe == 0)
                {
                    fprintf(stderr, "Cannot open pipe with popen(), error %d returned\n", errno);
                    exitTripe(0);
                }

                // Write file to pipe
                tarFile->writeTo(pipe, (char*)"pipe");

                // Wait for script to finish running
                pclose(pipe);

                // Write the revised file
                tarFile->outputTempFile(tmpFileName);
            }
        }
        else // No script, just pass through
        {
            // Write file to stdout, unaltered
            if (noOutput)
                tarFile->skipFile();
            else
            {
                tarFile->write(false);
                if (tarFile->isDir())
                    lastDirLen = strlen(tarFile->fileName());
            }
        }

        delete tarFile;
        tarFile = 0;
    }

    // If we were sending matched files to a temp dir, and the tar file ends on a file
    // that matches, then we are done with the input file but still need to
    // output the temp dir
    if (inMatchedDir && tmpDirName)
    {
        processScriptOnMatchedDir(script, tmpDirName, matchedDir);
        inMatchedDir = false;
        if (matchedDir)
            free(matchedDir);
    }

    // Write terminating block of zeros
    if (!noOutput)
        TarFile::writeTermBlocks();
    fclose(in);
    return 0;
}

void makeTmpName(char *s)
{
    struct timeval tv;
    if (gettimeofday(&tv, 0))
    {
        fprintf(stderr, "Error calling gettimeofday()\n");
        exitTripe(0);
    }
    sprintf(s, "%stripe-%d%d", tmpDirRoot, (int)tv.tv_sec % 10000, (int)tv.tv_usec);
}

void makeTempDirname()
{
    if (tmpDirName)
        free(tmpDirName);
    int len = strlen(tmpDirRoot) + 20;
    tmpDirName = (char*)malloc(len);
    if (!tmpDirName)
    {
        fprintf(stdout, "ERROR allocating %d bytes for tmpDirName\n.", len);
        exit(1);
    }
    makeTmpName(tmpDirName);
    strcat(tmpDirName, "/");
    tmpDirLength = strlen(tmpDirName);
}

void makeTempFilename()
{
    if (tmpFileName)
        free(tmpFileName);
    int len = strlen(tmpDirRoot) + 20;
    tmpFileName = (char*)malloc(len);
    if (!tmpFileName)
    {
        fprintf(stdout, "ERROR allocating %d bytes for tmpFileName\n.", len);
        exit(1);
    }
    makeTmpName(tmpFileName);
}

void setupSignalHandler()
{
    // Setup signals
    struct sigaction action;

    action.sa_handler = signalHandlerFunc;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    action.sa_flags |= SA_RESTART;

    if (sigaction(SIGHUP, &action, 0) > 0)
    {
        perror("sigaction: ");
        exit(1);
    }
    if (sigaction(SIGINT, &action, 0) > 0)
    {
        perror("sigaction: ");
        exit(1);
    }
    if (sigaction(SIGTERM, &action, 0) > 0)
    {
        perror("sigaction: ");
        exit(1);
    }
    if (sigaction(SIGPIPE, &action, 0) > 0)
    {
        perror("sigaction: ");
        exit(1);
    }
}

void signalHandlerFunc(int signal)
{
    sigCaught = signal;
}

void exitTripe(int signal)
{
    switch (signal)
    {
    case 0:
        // Do nothing
        break;
    case 1:
        fprintf(stderr, "Caught signal HUP\n");
        break;
    case 2:
        fprintf(stderr, "Caught signal INT\n");
        break;
    case 15:
        fprintf(stderr, "Caught signal TERM\n");
        break;
    case 13:
        fprintf(stderr, "Caught signal PIPE\n");
        break;
    default:
        fprintf(stderr, "Caught signal %d\n", signal);
        break;
    }
    fprintf(stderr, "Cleaning up...\n");
    // Delete tmp dir
    if (tmpDirName && !strncmp(tmpDirName, tmpDirRoot, strlen(tmpDirRoot)))
    {
        char cmdStr[110];
        sprintf(cmdStr, "rm -rf %s", tmpDirName);
        if (system(cmdStr) == -1)
        {
            fprintf(stderr, "ERROR: Failed to remove temp dir \"%s\"\n",
                    tmpDirName);
        }
    }
    fflush(0);
    // Quit for real
    exit(1);
    //raise(signal);
}

size_t fwrite_real(const void *ptr, size_t size, size_t nmemb,
                     FILE *stream)
{
    return fwrite(ptr, size, nmemb, stream);
}
